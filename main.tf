terraform {
 # Require Terraform at exactly version 1.0.4
 required_version = "= 1.0.4"
}


provider "aws" {
 region = "us-east-2"
 //version = "~> 2.0"
}

variable "server_port" {
 description = "The port the server will use for HTTP requests"
 type = number
 default = 8080 
}

variable "vm_names" {
 description = "VM names"
 type = list(string)
 default = ["vm1", "vm2"]
}

//basic output for single EC2 instance
/*output "public_ip" {
 value = aws_instance.example.public_ip
 description = "The public IP address of the web server"
}*/

//output for count
/*output "public_ips" {
 value = aws_instance.example[*].public_ip
 description = "The public IPs address of the web servers"
}*/

//output for-each
output "public_ips" {
 value = values(aws_instance.example)[*].public_ip
 description = "The public IPs address of the web servers"
}


resource "aws_instance" "example" {
    //count = 2
    for_each = toset(var.vm_names)
    ami = "ami-0c55b159cbfafe1f0"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance.id]

    user_data = <<-EOF
        #!/bin/bash
        echo "Hello, World" > index.html
        nohup busybox httpd -f -p ${var.server_port} &
        EOF
        //nohup busybox httpd -f -p 80 &
 
 tags = {
    //name for single EC2 instance
    /*Name = "terraform-example"*/
    //name for count
    //Name = var.vm_names[count.index]
    //name for-each
    Name = each.value
    }
}

resource "aws_security_group" "instance" {
 name = "terraform-example-instance"
 ingress {
 from_port =  var.server_port //8080
 to_port =  var.server_port //8080
 protocol = "tcp"
 cidr_blocks = ["0.0.0.0/0"]
 }
}
