include {
  path = find_in_parent_folders()
}

dependency "mysql" {
  config_path = "../../data-stores/mysql"
  
  mock_outputs = {
    mysql_address = "temporary-dummy-address"
    mysql_port    = "temporary-dummy-port"
  }

}

inputs = {
  mysql_address = dependency.mysql.outputs.mysql_address
  mysql_port = dependency.mysql.outputs.mysql_port
}